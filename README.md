# GIS (Gifs-Image-Search engine)
PoweredBy :
![ReactJs](https://user-images.githubusercontent.com/56060354/97405855-53c40280-191e-11eb-8fe5-8d7878b0b280.png)

![Author](https://img.shields.io/badge/author-Brijesh%20Burfal-lightgrey.svg?colorB=9900cc&style=flat-square)
![Author](https://img.shields.io/badge/author-Pratyush%20Kumar-lightgrey.svg?colorB=9900cc&style=flat-square)


GIS is a React app which fetches Images from Pixabay API and Gifs from Tenor API.

  - Simple and Minimal UI

# New Features!

  - DarkMode 
 
![darkm](https://user-images.githubusercontent.com/56060354/97354945-33635c00-18bc-11eb-8bfe-1025add74d9e.gif)

# Preview
## Image Search
![gifimageappss1](https://user-images.githubusercontent.com/56060354/97353532-2ba2b800-18ba-11eb-93b1-0d18c3ef7a93.png)

### API 
* [Pixabay]-  Over 1.8 million+ high quality stock images and videos.
* [Unsplash] - largest open collection of high-quality photos.
* [Tenor] - most relevant GIF search in over 35 languages worldwide.

### Tech

GIS uses a number of open source projects to work properly:

* [ReactJs] - HTML enhanced for web apps!
* [Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend






 



   [Pixabay]: <https://pixabay.com/api/docs/>
   [git-repo-url]: <https://github.com/BURFAL18/Gif-Image-Searcher>
   [node.js]: <http://nodejs.org>
   [ReactJs]: <http://reactjs.org>
   [BootStrap]:<https://getbootstrap.com/docs/4.0>
   [Unsplash]:<https://unsplash.com/documentation>
   [Tenor]:<https://tenor.com/gifapi/documentation>

